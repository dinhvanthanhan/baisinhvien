function getInfo() {
    var ma = document.getElementById("txtMaSV").value.trim();
    var ten = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();

    var sv = new SinhVien(ma, ten, email, matKhau, diemToan, diemLy, diemHoa);
    return sv;
}

function renderDssv(list) {
    var contentHTML = "";
    for (let i = 0; i < list.length; i++) {
        var currentSv = list[i];
        var contentTr = `<tr>
                            <td>${currentSv.ma}</td>
                            <td>${currentSv.ten}</td>
                            <td>${currentSv.email}</td>
                            <td>${currentSv.tinhDtb()}</td>
                            <td>
                                <button class="btn btn-danger" onclick="xoaSv(${currentSv.ma})">Xóa</button>
                                <button class="btn btn-primary" onclick="suaSv(${currentSv.ma})">Sửa</button>
                            </td>
                        </tr>`
        contentHTML += contentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function resetForm() {
    document.getElementById("formQLSV").reset();
}

function showInfo(sinhVien) {
    document.getElementById("txtMaSV").value = sinhVien.ma;
    document.getElementById("txtTenSV").value = sinhVien.ten;
    document.getElementById("txtEmail").value = sinhVien.email;
    document.getElementById("txtPass").value = sinhVien.matKhau;
    document.getElementById("txtDiemToan").value = sinhVien.diemToan;
    document.getElementById("txtDiemLy").value = sinhVien.diemLy;
    document.getElementById("txtDiemHoa").value = sinhVien.diemHoa;
}