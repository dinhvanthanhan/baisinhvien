const DSSV = "DSSV";
var dssv = [];

var dataJson = localStorage.getItem(DSSV);
if (dataJson) {

  var dataRaw = JSON.parse(dataJson);

  dssv = dataRaw.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemLy,
      item.diemToan,
      item.diemHoa
    );
  });

  renderDssv(dssv);
}

function saveLocalStorage() {
    // lưu xuống local
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
}

function themSv() {
    var newSv = getInfo();
    dssv.push(newSv);
    renderDssv(dssv);
    saveLocalStorage();
    resetForm();
}

function xoaSv(idSv) {
    var index = dssv.findIndex(function (sv) {
        return sv.ma == idSv;
    });
    if (index == -1) {
        return;
    }

    dssv.splice(index, 1);

    renderDssv(dssv);
}

function suaSv(idSv) {
    var index = dssv.findIndex(function (sv) {
        return sv.ma == idSv;
    });
    if (index == -1) {
        return;
    }

    var sv = dssv[index];

    showInfo(sv);

    document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
    var svEdit = getInfo();
    
    var index = dssv.findIndex(function (sv) {
        return sv.ma == svEdit.ma;
    });
    if (index == -1) {
        return;
    }

    dssv[index] = svEdit;
    renderDssv(dssv);
    saveLocalStorage();

    document.getElementById("txtMaSV").disabled = false;
}